﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Direct
{
    public partial class AddForm : Form
    {
        public Info Info { get; } = new Info();
        private FormState FormState;
        public AddForm(FormState formState, Info info = null)
        {
            InitializeComponent();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            MinimizeBox = false;//статическое масштабирование
            MaximizeBox = false;
            FormState = formState;
            switch (formState)
            {
                case FormState.Add:
                    {
                        Text = "Add human";                        
                        return;   
                    }                   
                case FormState.Edit:
                    {
                        textBox1.ReadOnly = true;
                        textBox1.Text = info.Number.ToString();
                        textBox2.Text = info.FIO;
                        textBox3.Text = info.Address;
                        button1.Text = "Edit";
                        Text = "Edit human";
                        break;
                    }
                case FormState.Delete:
                    {
                        textBox2.ReadOnly = true;
                        textBox3.ReadOnly = true;
                        Text = "Delete info";
                        button1.Text = "Delete";
                        break;
                    }
                case FormState.Search:
                    {                        
                        button1.Text = FormState.ToString();
                        textBox2.ReadOnly = true;
                        textBox3.ReadOnly = true;
                        Text = "Search human";
                        break;
                    }
                case FormState.Display:
                    {
                     
                        textBox1.ReadOnly = true;
                        textBox1.Text = info.Number.ToString();
                        textBox2.Text = info.FIO;
                        textBox3.Text = info.Address;
                        textBox2.ReadOnly = true;
                        textBox3.ReadOnly = true;
                        button1.Visible = false;
                        break;
                    }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {           
            try
            {
                Info.Number = long.Parse(textBox1.Text);
                if (FormState == FormState.Add || FormState == FormState.Edit)
                {
                    button1.Text = "Add";
                    Info.FIO = textBox2.Text;
                    Info.Address = textBox3.Text;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Поле с номером телефона не заполнено. Повторите попытку");
            }          
            Close();
        }
    }
}
